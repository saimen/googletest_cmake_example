cmake_minimum_required(VERSION 3.11)

project(googletest_test
            VERSION 0.0.0
            DESCRIPTION "Simple Googletest with CMake demonstration"
            LANGUAGES CXX)

find_package(GTest REQUIRED)
include(GoogleTest)

add_executable(myGtestTest src/foo.cpp)

target_link_libraries(myGtestTest GTest::GTest GTest::Main)

enable_testing()
gtest_discover_tests(myGtestTest)

