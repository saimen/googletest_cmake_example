#include <gtest/gtest.h>

unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST(FactorialTest, Factorial_SeveralValues ) {
    ASSERT_EQ(1, Factorial(1));
    ASSERT_EQ(2, Factorial(2));
    ASSERT_EQ(6, Factorial(3));
    ASSERT_EQ(3628800, Factorial(10));
}

TEST(FactorialTest, Factorial_Fail ) {
    ASSERT_EQ(2,Factorial(1));
}
